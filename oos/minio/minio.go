package minio

import (
	"gitee.com/chenhonghua/ginorigin/log"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

var (
	defaultRegion string = "cn-north-1"
	minioConfig   MinioConfig
	minioClient   *minio.Client
)

// minio配置
type MinioConfig struct {
	Enable          bool   `mapstructure:"enable" json:"enable" yaml:"enable"`                                // 是否开启
	Endpoint        string `mapstructure:"endpoint" json:"endpoint" yaml:"endpoint"`                          // api节点url，api端口默认为9000，管理页面端口默认9001
	AccessKeyID     string `mapstructure:"access-key-id" json:"accessKeyID" yaml:"access-key-id"`             // 访问ID，在users的ServiceAccount界面生成，可以指定值，也可以随机生成
	SecretAccessKey string `mapstructure:"secret-access-key" json:"secretAccessKey" yaml:"secret-access-key"` // 密钥（与AccessKeyID成对）
	UseSSL          bool   `mapstructure:"use-ssl" json:"useSSL" yaml:"use-ssl"`                              // 是否开启ssl
	Bucket          string `mapstructure:"bucket" json:"bucket" yaml:"bucket"`                                // 桶名
}

func (c MinioConfig) Load() {
	if !c.Enable {
		return
	}
	minioConfig = c
	// http://docs.minio.org.cn/docs/master/golang-client-api-reference
	log.Debugf("minio模块:%v\n", minioConfig)
	var err error
	minioClient, err = minio.New(minioConfig.Endpoint, &minio.Options{
		Region: defaultRegion,
		Creds:  credentials.NewStaticV4(minioConfig.AccessKeyID, minioConfig.SecretAccessKey, ""),
		Secure: minioConfig.UseSSL,
	})
	if err != nil {
		log.Fatal(err)
	}
	Default = bucket{name: c.Bucket}
	err = Default.Exists()
	if err != nil {
		log.Fatal(err)
	}
	// 原本此处想要设计一个调整minio访问策略的功能，不过，相关规则并不明确，且每次启动都会设置，那就太坑了，暂缓
}
