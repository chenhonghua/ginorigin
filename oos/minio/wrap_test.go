package minio

import (
	"context"
	"encoding/json"
	"io"
	"log"
	"os"
	"path"
	"testing"
)

func loadTestConfig() {
	MinioConfig{
		Enable:          true,
		Endpoint:        "my.docker.server:9000",
		AccessKeyID:     "J8P16YDZQU21YMS46VWP",
		SecretAccessKey: "q1tNjmriWXoTkaDLFnWKfPERVCsRvxYFATsdDQHg",
		UseSSL:          false,
		Bucket:          "edusys",
	}.Load()
}

func TestInit(t *testing.T) {
	loadTestConfig()
}

type oosobject struct {
	Path     string `json:"path" gorm:"column:relation_path;type:varchar(512);not null;unique;index;"` // minio上存放路径
	Size     int64  `json:"size"`                                                                      // 对象大小
	MineType string `json:"mineType"`                                                                  // 对象文件类型
	FileName string `json:"fileName"`                                                                  // 文件名
}

func TestPut(t *testing.T) {
	loadTestConfig()
	filename := "deepin-theme-2.jpg"
	localpath := "/home/chenhonghua/Pictures/Wallpapers"
	destpath := "/testminio"
	opts := putOption

	fileReader, _ := os.Open(path.Join(localpath, filename))

	uploadInfo, err := minioClient.PutObject(context.Background(), minioConfig.Bucket, path.Join(destpath, filename), fileReader, int64(-1), opts)
	if err != nil {
		log.Fatalf("文件上传发生错误: %#v\n", err)
		return
	}
	log.Printf("文件上传成功:%#v\n", uploadInfo)

	obj := oosobject{
		Path:     uploadInfo.Key,
		Size:     uploadInfo.Size,
		MineType: path.Ext(uploadInfo.Key),
		FileName: filename,
	}
	j, _ := json.Marshal(obj)
	log.Printf("obj对象信息：%s\n", string(j))
}

func TestGet(t *testing.T) {
	loadTestConfig()
	s := `{"path":"testminio/deepin-theme-2.jpg","size":2185104,"mineType":".jpg","filename":"deepin-theme-2.jpg"}`
	oo := oosobject{}
	json.Unmarshal([]byte(s), &oo)
	log.Printf("解析字符串完成，obj对象信息：%#v\n", oo)

	localpath := "./pic"
	opts := getOption

	obj, err := minioClient.GetObject(context.Background(), minioConfig.Bucket, oo.Path, opts)
	if err != nil {
		log.Fatalf("获取文件发生错误: %#v\n", err)
		return
	}
	oi, err := obj.Stat()
	if err != nil {
		log.Fatalf("获取文件信息发生错误: %#v\n", err)
		return
	}
	log.Printf("minio对象信息，object:%#v，objectinfo:%#v\n", oo, oi)

	// data := make([]byte, oi.Size)
	// readFull(obj, data)

	// f,_:=os.Create(path.Join(localpath, oo.FileName))
	filePart, err := os.OpenFile(path.Join(localpath, oo.FileName), os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		log.Fatalf("写入本地文件发生错误: %#v\n", err)
		return
	}
	io.Copy(filePart, obj)

}

var readFull = func(r io.Reader, buf []byte) (n int, err error) {
	for n < len(buf) && err == nil {
		var nn int
		nn, err = r.Read(buf[n:])
		if err == io.ErrUnexpectedEOF && nn == 0 {
			err = io.EOF
		}
		n += nn
	}
	if n >= len(buf) {
		err = nil
	} else if n > 0 && err == io.EOF {
		err = io.ErrUnexpectedEOF
	}
	return
}
