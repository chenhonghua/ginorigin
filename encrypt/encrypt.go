package encrypt

import (
	"crypto/md5"
	"encoding/hex"
)

func Md5Encrypt(src []byte, salt []byte) string {
	m := md5.New()
	m.Write(src)
	b := m.Sum(salt)
	return hex.EncodeToString(b)
}
