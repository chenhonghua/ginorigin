package sfs

import (
	"net/http"
	pathpkg "path"
	"strings"
)

type ServerFileSystem struct {
	http.FileSystem
}

func (sys ServerFileSystem) Exists(prefix string, filepath string) bool {
	if p := strings.TrimPrefix(filepath, prefix); len(p) < len(filepath) {
		fullName := pathpkg.Join("/", p)
		_, err := sys.Open(fullName)
		return err == nil
	}
	return false
}
