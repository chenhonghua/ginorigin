package sfs

import (
	"bytes"
	"fmt"
	"os"
	"time"
)

// FileInfo is a static definition of an uncompressed file (because it's not worth gzip compressing).
type FileInfo struct {
	name    string
	modTime time.Time
	content []byte
}

func (f *FileInfo) Readdir(count int) ([]os.FileInfo, error) {
	return nil, fmt.Errorf("cannot Readdir from file %s", f.name)
}
func (f *FileInfo) Stat() (os.FileInfo, error) { return f, nil }

func (f *FileInfo) NotWorthGzipCompressing() {}

func (f *FileInfo) Name() string       { return f.name }
func (f *FileInfo) Size() int64        { return int64(len(f.content)) }
func (f *FileInfo) Mode() os.FileMode  { return 0444 }
func (f *FileInfo) ModTime() time.Time { return f.modTime }
func (f *FileInfo) IsDir() bool        { return false }
func (f *FileInfo) Sys() interface{}   { return nil }

// File is an opened file instance.
type File struct {
	*FileInfo
	*bytes.Reader
}

func (f *File) Close() error {
	return nil
}
