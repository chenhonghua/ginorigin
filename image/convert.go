package image

import (
	"bytes"
	"image"
	"image/draw"
	"image/gif"
	"image/jpeg"
	"image/png"
	"net/http"
)

// []byte -> RGBA
func Bytes2RGBA(d []byte) (*image.RGBA, error) {
	backgroud, e := Bytes2Image(d)
	if e != nil {
		return nil, e
	}
	return Image2RGBA(backgroud), nil
}

// RGBA -> []byte
func RGBA2Bytes(rgba *image.RGBA) ([]byte, error) {
	img := RGBA2Image(rgba)
	return Image2Bytes(img)
}

// []byte -> Image
func Bytes2Image(d []byte) (img image.Image, e error) {
	switch http.DetectContentType(d) {
	case "image/jpeg", "image/jpg":
		return jpeg.Decode(bytes.NewReader(d))
	case "image/gif":
		return gif.Decode(bytes.NewReader(d))
	case "image/png":
		return png.Decode(bytes.NewReader(d))
	default:
		return nil, e
	}
}

// Image -> []byte
func Image2Bytes(img image.Image) (d []byte, e error) {
	buf := new(bytes.Buffer)
	if e := png.Encode(buf, img); e != nil {
		return nil, e
	}
	return buf.Bytes(), nil
}

// Image -> RGBA
func Image2RGBA(img image.Image) *image.RGBA {
	point := img.Bounds().Max
	rect := image.Rect(0, 0, point.X, point.Y)                     // 根据图片尺寸生成同尺寸的矩形
	des := image.NewRGBA(rect)                                     // 创建底板（没有内容）
	draw.Draw(des, des.Bounds(), img, img.Bounds().Min, draw.Over) // 将图片内容绘制到底板上
	return des
}

// RGBA -> Image
func RGBA2Image(rgba *image.RGBA) image.Image {
	return rgba.SubImage(rgba.Rect)
}
