package image

import (
	"image"
	"image/color"
	"os"

	"github.com/golang/freetype"
	"github.com/golang/freetype/truetype"
	"golang.org/x/image/math/fixed"
)

// 图片绘字信息
type Text struct {
	Text string
	X    int // 文本区域左上角X坐标
	Y    int // 文本区域左上角Y坐标
}

type Drawer struct {
	rgba    *image.RGBA
	context *freetype.Context
}

func NewDrawer(data []byte) (d *Drawer, e error) {
	d = new(Drawer)
	if d.rgba, e = Bytes2RGBA(data); e != nil { // 源图像字节转rgba格式结构体
		return nil, e
	}
	d.context = freetype.NewContext()  // 创建绘图上下文
	d.context.SetClip(d.rgba.Bounds()) // 设置区域
	d.context.SetDst(d.rgba)           // 设置目标图像
	d.SetFontColor(color.RGBA{A: 255})
	return d, nil
}

func (d *Drawer) SetDPI(dpi float64) {
	if dpi < 0 {
		dpi = 72
	}
	d.context.SetDPI(dpi)
}

// 设置字体样式
func (d *Drawer) SetFont(ttfPath string) error {
	if fontFile, e := os.ReadFile(ttfPath); e != nil {
		return e
	} else if ttf, e := truetype.Parse(fontFile); e != nil {
		return e
	} else {
		d.context.SetFont(ttf)
		return nil
	}
}

// 设置字体颜色
func (d *Drawer) SetFontColor(color color.Color) {
	d.context.SetSrc(image.NewUniform(color))
}

// 设置字体大小
func (d *Drawer) SetFontSize(size float64) {
	if size < 0 {
		size = 12
	}
	d.context.SetFontSize(size)
}

// 绘字
func (d Drawer) DrawString(text Text) (fixed.Point26_6, error) {
	return d.context.DrawString(text.Text, freetype.Pt(text.X, text.Y))
}

func (d Drawer) Output() ([]byte, error) {
	return RGBA2Bytes(d.rgba)
}
