package ternary

// 仿三元表达式
func Ternary[T interface{}](condition bool, trueVal, falseVal T) T {
	if condition {
		return trueVal
	} else {
		return falseVal
	}
}
