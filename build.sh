#!/bin/bash

set -x
projectdir=$(cd `dirname $0`;pwd)

# 项目打包编译
export CGO_ENABLED=0
export GOARCH=amd64
export GOOS=linux
# 生成swagger文档
go install github.com/swaggo/swag/cmd/swag && swag init --parseDependency --parseInternal

# gopath打包
# go clean && go get && go build -v -a -o ginorigin.linux.x86_64
# 如果嫌gopath太慢(比如每次容器打包都会重新拉取一份依赖)，可以使用vendor进行打包，vendor会像node一样，将依赖放在项目根目录下的指定目录里
go mod tidy && go mod vendor && go build -mod=vendor -v -a -o ginorigin.linux.x86_64
