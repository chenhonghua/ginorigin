@echo off
set CGO_ENABLED = 0
set GOARCH = amd64
set GOOS = windows
set GO15VENDOREXPERIMENT = 1
@REM go clean
@REM go get
@REM 生成swagger文档
@REM windows上很慢
@REM go install github.com/swaggo/swag/cmd/swag
@REM swag init
@REM swag init --pd --parseVendor
@REM 打包
go mod tidy -compat=1.17
go mod vendor
go build -mod=vendor -v -a -o ginorigin.windows.x86_64.exe