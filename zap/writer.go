package zap

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"time"

	"go.uber.org/zap/zapcore"
)

// 判断日志文件
func (z ZapConfig) logFile() string {
	// 日志目录
	z.Directory, _ = filepath.Abs(z.Directory)
	fi, err := os.Stat(z.Directory)
	if err == nil { // 存在同名文件或文件夹
		if !fi.IsDir() {
			panic(errors.New("存在同名文件"))
		}
	} else if os.IsNotExist(err) { // 无同名文件或文件夹
		fmt.Printf("创建日志目录[%v]\n", z.Directory)
		err = os.Mkdir(z.Directory, os.ModePerm)
		if err != nil {
			s := fmt.Sprintf("创建日志目录[%s]失败，错误信息：%s\n", z.Directory, err.Error())
			err = errors.New(s)
			panic(err)
		}
	}
	// 日志文件
	if len(z.FileNamePrefix) == 0 {
		z.FileNamePrefix = "server"
	}
	return fmt.Sprintf("%s/%s_%s.log", z.Directory, z.FileNamePrefix, time.Now().Format("20060102150405"))
}

// 文件输出流
func (z ZapConfig) fileWriteSyncer(fileName string) (w zapcore.WriteSyncer) {
	l := z.ZapCompress.zapCompressLogger(fileName)
	if z.LogInConsole {
		w = zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout), zapcore.AddSync(l))
	} else {
		w = zapcore.AddSync(l) // 使用file-rotatelogs进行日志分割
	}
	return w
}
