package zap

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// 性能说明：
// 日志判定（是否记录）耗时约30-50us
// 日志记录耗时约100-200us
// Any方法解析耗时约10us（受对象复杂度影响较大）

// 转换
// zap.LOGGER.Debut(msg, zap.Any(name1, value1), zap.Any(name2, value2))
func Any(k string, v interface{}) zapcore.Field {
	return zap.Any(k, v)
}

// 转换
// zap.LOGGER.Error(msg, zap.Error(err1), zap.Error(err2))
func Error(err error) zapcore.Field {
	return zap.Error(err)
}

func PanicIfErr(err error) {
	if err != nil {
		LOGGER.Error(err.Error(), Error(err))
		panic(err)
	}
}
