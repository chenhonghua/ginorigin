package zap

import (
	"github.com/natefinch/lumberjack"
)

type ZapCompressConfig struct {
	Enable     bool `mapstructure:"enable" json:"enable" yaml:"enable"`               // 是否开启
	MaxSize    int  `mapstructure:"max-size" json:"MaxSize" yaml:"max-size"`          // 在进行切割之前，日志文件的最大大小（以MB为单位）
	MaxBackups int  `mapstructure:"max-backups" json:"maxBackups" yaml:"max-backups"` // 保留旧文件的最大个数
	MaxAge     int  `mapstructure:"max-age" json:"maxAge" yaml:"max-age"`             // 保留旧文件的最大天数
}

// 配置日志分割压缩输出流
func (zc ZapCompressConfig) zapCompressLogger(fileName string) *lumberjack.Logger {
	if zc.MaxSize == 0 { // 默认值
		zc.MaxSize = 100
		zc.MaxBackups = 100
		zc.MaxSize = 30
	}
	return &lumberjack.Logger{
		Filename:   fileName,      // 日志文件的位置
		Compress:   zc.Enable,     // 是否压缩/归档旧文件
		MaxSize:    zc.MaxSize,    // 在进行切割之前，日志文件的最大大小（以MB为单位）
		MaxBackups: zc.MaxBackups, // 保留旧文件的最大个数
		MaxAge:     zc.MaxAge,     // 保留旧文件的最大天数
	}
}
