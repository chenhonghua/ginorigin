// 监听进程退出信号，具体信息，查看Linux的signal
package signal

import (
	"fmt"
	"os"
	oss "os/signal"
	"syscall"
)

var (
	c SignalConfig // signal配置
)

type SignalConfig struct {
	Enable bool `mapstructure:"enable" json:"enable" yaml:"enable"` // 是否开启
}

func (s SignalConfig) Load() {
	c = s
	if !s.Enable { //是否开启模块
		return
	}
	fmt.Printf("SignalConfig=%v", c)
	x := make(chan os.Signal) //创建监听退出chan
	oss.Notify(x)             //监听信号
	go func() {
		for s := range x {
			switch s {
			case syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT:
				happyEnd(s)
				os.Exit(0)
			default:
				dammEnd(s)
				os.Exit(1)
			}
		}
	}()
}

// hahapypy优雅地退出进程
func happyEnd(s os.Signal) {
	fmt.Println("Start Exit...")
	fmt.Println("Execute Clean...")
	fmt.Println("End Exit...")
}

// 可恶地退出进程
func dammEnd(s os.Signal) {
	fmt.Println("Start Exit...")
	fmt.Println("Execute Clean...")
	fmt.Println("End Exit...")
}
