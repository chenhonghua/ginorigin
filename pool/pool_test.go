package pool

import (
	"math/rand"
	"sync"
	"sync/atomic"
	"testing"
	"time"

	"gitee.com/chenhonghua/ginorigin/log"

	"github.com/bytedance/gopkg/util/gopool"
)

// 失败：并发执行会产生虚执行，导致部分任务未执行完整就退出
// func TestPool(_ *testing.T) {
// 	var resultMap sync.Map
// 	for range 1 {
// 		log.Info("开始")
// 		resultMap = sync.Map{}
// 		st := time.Now().UnixMilli()
// 		p, _ := NewPool(4)
// 		c := uint64(0)
// 		size := 100
// 		var completeCount uint64 = 0
// 		for i := range size {
// 			resultMap.Store(i, 0)
// 			p.Put(&Task{
// 				Execute: func(args ...interface{}) {
// 					v := rand.Intn(10)
// 					c += uint64(v)
// 					// time.Sleep(time.Millisecond * time.Duration(v))
// 					atomic.AddUint64(&completeCount, 1)
// 					resultMap.Delete(i)
// 				},
// 				Args: []interface{}{i},
// 			})
// 		}
// 		et := time.Now().UnixMilli()
// 		log.Infof("完成[%d/%d]: %dms / %dms", completeCount, size, et-st, c)
// 		resultMap.Range(func(key, value any) bool {
// 			log.Warn(key)
// 			return true
// 		})
// 	}
// }

// https://github.com/bytedance/gopkg/blob/develop/util/gopool/pool_test.go
// 字节得携程池目前测试可用，且精度较高，未出现上面得虚执行问题。
// 归根结底应该是sync.WaitGroup的使用
func TestPool2(_ *testing.T) {
	var resultMap sync.Map
	for range 10 {
		log.Info("开始")
		resultMap = sync.Map{}
		st := time.Now().UnixMilli()
		c := uint64(0)
		size := 10000000
		var completeCount uint64 = 0
		gopool.SetCap(8)
		var wg sync.WaitGroup
		for i := range size {
			wg.Add(1)
			resultMap.Store(i, i)
			gopool.Go(func() {
				defer wg.Done()
				resultMap.Store(i, i)
				atomic.AddUint64(&c, uint64(rand.Intn(10)))
				atomic.AddUint64(&completeCount, 1)
				resultMap.Delete(i)
			})
		}
		wg.Wait()
		et := time.Now().UnixMilli()
		log.Infof("完成[%d/%d]: %dms / %dms", completeCount, size, et-st, c)
		resultMap.Range(func(key, value any) bool {
			log.Warnf("miss: %v", key)
			return true
		})
	}
}
