package timezone

import "time"

var (
	LOCATION *time.Location = time.FixedZone("CST", 8*3600) // 全局时区信息，东八区
)

type TimeZone struct {
	Name   string `mapstructure:"name" json:"name" yaml:"name"`       // 时区名：CST,UTC
	Offset int    `mapstructure:"offset" json:"offset" yaml:"offset"` // 时区偏移量（秒）
}

func (tz TimeZone) Load() {
	if len(tz.Name) == 0 {
		return
	}
	LOCATION = time.FixedZone(tz.Name, tz.Offset)
}
