package redis

import (
	redis "github.com/go-redis/redis/v8"
)

var (
	Client *redis.Client // 默认redis客户端
)

// 初始化默认redis连接
//
// 注意：
//
//	如果您的项目中需要用到不同的redis客户端，最好不要使用此函数，
//	而是使用Redis{}.NewClient()生成对应的redis客户端，然后自行保持和管理客户端。
func LoadClient(host string, port int, password string, db int) (e error) {
	r := Redis{
		Host:     host,
		Port:     port,
		Password: password,
		DB:       db,
	}
	Client, e = r.NewClient()
	return e
}
