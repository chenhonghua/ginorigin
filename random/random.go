package random

import (
	"gitee.com/chenhonghua/ginorigin/encrypt"
	"fmt"
	"math/rand"
	"time"
)

func RandomString() string {
	s := fmt.Sprintf("%d%d", time.Now().UnixNano(), rand.Int63())
	return encrypt.Md5Encrypt([]byte(s), nil)
}
