package i18n

import (
	_ "embed"

	goi18n "github.com/nicksnyder/go-i18n/v2/i18n"
	"golang.org/x/text/language"
)

// I18nMessageManage 结构体用于管理国际化消息。
type I18nMessageManage struct {
	Bundle          *goi18n.Bundle                              // 国际化资源包
	messages        map[language.Tag]map[string]*goi18n.Message // 一个映射，键为语言标签，值为该语言下的消息映射
	defaultLanguage language.Tag                                // 默认的语言
}

// NewI18nMessageManage 创建一个新的国际化消息管理实例。
// 参数 defaultLanguage 指定了默认的语言标签。
// 返回一个初始化后的 I18nMessageManage 实例，其中包含了基于指定默认语言的新建 Bundle。
func NewI18nMessageManage(defaultLanguage language.Tag) I18nMessageManage {
	l := I18nMessageManage{Bundle: goi18n.NewBundle(defaultLanguage)}
	l.defaultLanguage = defaultLanguage
	return l
}

// AddMessages 向管理器中添加消息。
//
// 参数：
//   - lang: 语言。
//   - messages: 消息。
func (manage *I18nMessageManage) AddMessages(lang language.Tag, messages map[string]string) error {
	if len(manage.messages) == 0 {
		manage.messages = make(map[language.Tag]map[string]*goi18n.Message)
	}
	for key, s := range messages {
		msg := goi18n.Message{
			ID:    key,
			Other: s,
		}
		manage.Bundle.MustAddMessages(lang, &msg)
		tagMessages := manage.messages[lang]
		if len(tagMessages) == 0 {
			tagMessages = make(map[string]*goi18n.Message)
		}
		tagMessages[msg.ID] = &msg
		manage.messages[lang] = tagMessages
	}
	return nil
}

// String 根据默认语言环境获取本地化的字符串。
// 它调用 StringWithLanguage 方法，传入消息ID和默认语言环境。
// 参数：
//   - id: 消息的唯一标识符。
//   - args: 可选参数，用于字符串格式化。
func (manage *I18nMessageManage) String(id string) string {
	return manage.StringWithLanguage(id, manage.defaultLanguage)
}

// StringWithLanguage 根据指定的语言标签和参数返回本地化的字符串。
// 如果提供的语言标签为空，则使用默认语言。
func (manage *I18nMessageManage) StringWithLanguage(id string, lang language.Tag) string {
	if len(lang.String()) == 0 {
		lang = manage.defaultLanguage
	}
	if s, _, e := goi18n.NewLocalizer(manage.Bundle, lang.String()).LocalizeWithTag(&goi18n.LocalizeConfig{MessageID: id}); e != nil {
		panic(e)
	} else {
		return s
	}
}
