/*
 * @Author: thepoy
 * @Email: thepoy@163.com
 * @File Name: default.go
 * @Created: 2021-05-16 09:39:17
 * @Modified: 2021-05-16 19:54:27
 */

package log

import (
	"strings"

	"gitee.com/chenhonghua/ginorigin/log/impl"
)

var DefaultLogger *impl.Logger = impl.NewLogger()

func SetLogLevel(level string) {
	level = strings.ToLower(level)
	switch level {
	case "trace":
		DefaultLogger.SetLogLevel(impl.AllLevel)
	case "debug":
		DefaultLogger.SetLogLevel(impl.DebugLevel)
	case "info":
		DefaultLogger.SetLogLevel(impl.InfoLevel)
	case "warn":
		DefaultLogger.SetLogLevel(impl.WarnLevel)
	case "error":
		DefaultLogger.SetLogLevel(impl.ErrorLevel)
	case "fatal":
		DefaultLogger.SetLogLevel(impl.FatalLevel)
	default:
		DefaultLogger.SetLogLevel(impl.InfoLevel)
	}
}

// Trace 默认 Trace 方法
func Trace(v ...interface{}) {
	DefaultLogger.Trace(v...)
}

// Tracef 默认 Tracef 方法
func Tracef(format string, v ...interface{}) {
	DefaultLogger.Tracef(format, v...)
}

// Info 默认 Info 方法
func Info(v ...interface{}) {
	DefaultLogger.Info(v...)
}

// Infof 默认 Infof 方法
func Infof(format string, v ...interface{}) {
	DefaultLogger.Infof(format, v...)
}

// Debug 默认 Debug 方法
func Debug(v ...interface{}) {
	DefaultLogger.Debug(v...)
}

// Debugf 默认 Debugf 方法
func Debugf(format string, v ...interface{}) {
	DefaultLogger.Debugf(format, v...)
}

// Warn 默认 Warn 方法
func Warn(v ...interface{}) {
	DefaultLogger.Warn(v...)
}

// Warnf 默认 Warnf 方法
func Warnf(format string, v ...interface{}) {
	DefaultLogger.Warnf(format, v...)
}

// Error 默认 Error 方法
func Error(v ...interface{}) {
	DefaultLogger.Error(v...)
}

// Errorf 默认 Errorf 方法
func Errorf(format string, v ...interface{}) {
	DefaultLogger.Errorf(format, v...)
}

// Fatal 默认 Fatal 方法
func Fatal(v ...interface{}) {
	DefaultLogger.Fatal(v...)
}

// Fatalf 默认 Fatalf 方法
func Fatalf(format string, v ...interface{}) {
	DefaultLogger.Fatalf(format, v...)
}
