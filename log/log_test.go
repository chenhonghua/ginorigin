package log

import (
	"testing"
	"time"
)

func testCell() {
	Trace("trace")
	Debug("debug")
	Info("info")
	Warn("warn")
	Error("error")
}

func TestMain1(_ *testing.T) {
	SetLogLevel("trace")
	Trace("开始测试")
	testCell()
	Trace("---------------------------------------------------------------------------------------------")
}

func TestMain2(_ *testing.T) {
	// var threadNum int = 1
	// var wait sync.WaitGroup = sync.WaitGroup{}
	// wait.Add(threadNum)
	// Trace("开始测试")
	for j := 10; j < 1; j++ {
		go func() {
			for i := 0; i < 1; i++ {
				testCell()
			}
			// wait.Done()
		}()
	}
	// wait.Wait()
	time.Sleep(time.Second*2)
	Trace("---------------------------------------------------------------------------------------------")
}
