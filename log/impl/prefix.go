package impl

var (
	FatalPrefix = Prefix{Plain: []byte("[FATAL] "), Color: colorPurple, File: true, ShortFile: true}
	ErrorPrefix = Prefix{Plain: []byte("[ERROR] "), Color: colorRed, File: true, ShortFile: true}
	WarnPrefix  = Prefix{Plain: []byte("[WARN]  "), Color: colorOrange, File: true, ShortFile: true}
	InfoPrefix  = Prefix{Plain: []byte("[INFO]  "), Color: colorGreen, File: true, ShortFile: true}
	DebugPrefix = Prefix{Plain: []byte("[DEBUG] "), Color: colorDefault, File: true, ShortFile: true}
	TracePrefix = Prefix{Plain: []byte("[TRACE] "), Color: colorGray, File: true, ShortFile: true}
)

func init() {
	FatalPrefix.colorPlain = mixer(FatalPrefix.Plain, FatalPrefix.Color)
	ErrorPrefix.colorPlain = mixer(ErrorPrefix.Plain, ErrorPrefix.Color)
	WarnPrefix.colorPlain = mixer(WarnPrefix.Plain, WarnPrefix.Color)
	InfoPrefix.colorPlain = mixer(InfoPrefix.Plain, InfoPrefix.Color)
	DebugPrefix.colorPlain = mixer(DebugPrefix.Plain, DebugPrefix.Color)
	TracePrefix.colorPlain = mixer(TracePrefix.Plain, TracePrefix.Color)
}

// Prefix 日志信息的前缀和颜色
type Prefix struct {
	Plain      []byte
	Color      []byte
	colorPlain []byte
	File       bool
	ShortFile  bool
	ShowFunc   bool
}

func (p *Prefix) ColorData(data []byte) []byte {
	return mixer(data, p.Color)
}
