package impl

import "io"

type FdWriter interface {
	io.Writer
	Fd() uintptr
}
