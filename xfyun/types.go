package xfyun

// 用于上传公共参数
type common struct {
	AppId string `json:"app_id"` // 平台申请的appid
}

// 用于上传业务参数
type business struct {
	Ent string `json:"ent"` // 请求引擎类型
	Aue string `json:"aue"` // 预留压缩格式
}

