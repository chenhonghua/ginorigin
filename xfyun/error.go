package xfyun

import "errors"

var (
	ErrSys                         = errors.New("系统错误")
	ErrInvalidParams               = errors.New("非法参数")
	ErrServerResponseAnalyzeFailed = errors.New("无法解析服务器响应")
	ErrServerResponseEmpty         = errors.New("服务器响应数据为空")
	ErrRequestAuthorizeEmpty       = errors.New("缺少authorization参数。检查是否有authorization参数，详情见authorization参数详细生成规则。")
	ErrRequestAuthorizeInvalid     = errors.New("签名参数解析失败。检查签名的各个参数是否有缺失是否正确，特别确认下复制的api_key是否正确。")
	ErrRequestAuthorizeNotMatch    = errors.New("签名校验失败。签名验证失败，可能原因有很多。1. 检查api_key,api_secret 是否正确。2.检查计算签名的参数host，date，request-line是否按照协议要求拼接。3. 检查signature签名的base64长度是否正常(正常44个字节)。")
	ErrRequestTimeNotMatch         = errors.New("时钟偏移校验失败。检查服务器时间是否标准，相差5分钟以上会报此错误。")
	ErrRequestIPNotAllow           = errors.New("IP白名单校验失败。可在控制台关闭IP白名单，或者检查IP白名单设置的IP地址是否为本机外网IP地址。")

	ErrXFFileSizeTooLarge = errors.New("文件大小超过限制。确保文件base64编码后大小不超过4MB。")
)
