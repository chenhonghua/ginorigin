package xfyun

import (
	"os"
	"testing"

	"gitee.com/chenhonghua/ginorigin/log"
)

func TestITR(_ *testing.T) {
	d, e := os.ReadFile("./img/testitr.jpg")
	if e != nil {
		log.Fatal(e)
	}
	res, e := ITR(d)
	if e != nil {
		log.Fatal(e)
	}
	log.Infof("body 1: %#v", res)
}
