package xfyun

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"net/http"

	"gitee.com/chenhonghua/ginorigin/env"
)

var (
	appid  = env.String("XFYUN_APPID")
	apikey = env.String("XFYUN_APIKEY")
	secret = env.String("XFYUN_SECRET")
	client = &http.Client{}
)

// []byte -> b64string
func b64(b []byte) string {
	return base64.StdEncoding.EncodeToString(b)
}

// sha256 -> b64
func sha256B64(d []byte) string {
	//进行sha256签名
	sha := sha256.New()
	sha.Write(d)
	encodeData := sha.Sum(nil)
	//经过base64转换
	return b64(encodeData)
}

// hmac -> b64
func hmacB64(s string, key []byte) string {
	mac := hmac.New(sha256.New, key)
	mac.Write([]byte(s))
	encodeData := mac.Sum(nil)
	return b64(encodeData)
}
