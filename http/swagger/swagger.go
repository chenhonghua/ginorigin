package swagger

import (
	gs "github.com/swaggo/gin-swagger"
	gsf "github.com/swaggo/gin-swagger/swaggerFiles"

	"gitee.com/chenhonghua/ginorigin/http/router"
	"gitee.com/chenhonghua/ginorigin/log"
	// _ "gitee.com/chenhonghua/ginorigin/docs"
)

var (
	c SwaggerConfig
)

type SwaggerConfig struct {
	Enable       bool   `mapstructure:"enable" json:"enable" yaml:"enable"`                     // 是否开启
	RelativePath string `mapstructure:"relative-path" json:"relativePath" yaml:"relative-path"` // 默认uri
}

func (s SwaggerConfig) Load() {
	c = s
	if !s.Enable {
		return
	}
	if len(s.RelativePath) == 0 {
		s.RelativePath = "/swagger/*any"
	}
	log.Debugf("启用swagger模块:%#v\n", c)
	router.GetRouter().GET(s.RelativePath, gs.WrapHandler(gsf.Handler))
}
