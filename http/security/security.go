package security

import (
	"gitee.com/chenhonghua/ginorigin/database"
	"gitee.com/chenhonghua/ginorigin/http/security/role"
	"gitee.com/chenhonghua/ginorigin/log"
)

var securityConfig SecurityConfig

type SecurityConfig struct {
	Enable           bool `mapstructure:"enable" json:"enable" yaml:"enable"`                                   // 是否开启
	AllowCors        bool `mapstructure:"allow_cors" json:"allow_cors" yaml:"allow_cors"`                       // 是否允许跨域
	EnableRedisCache bool `mapstructure:"enable-redis-cache" json:"enableRedisCache" yaml:"enable-redis-cache"` // 是否开启redis缓存
}

func (c SecurityConfig) Load() {
	if !c.Enable {
		return
	}
	securityConfig = c
	log.Debug("开始创建security模块数据库表")
	if e := database.GetConnection().Migrator().AutoMigrate(role.Role{}, role.RolePolicy{}, role.UserRole{}); e != nil {
		log.Fatal(e)
	}
}
