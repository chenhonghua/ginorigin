package security

import (
	"errors"

	"gitee.com/chenhonghua/ginorigin/http/restful"
)

var (
	ErrNotActive             = errors.New("未启用security模块")
	ErrNotActiveInvalidParam = errors.New("参数错误，未启用security模块")
	ErrRegistFailed          = errors.New("无法注册：错误的uri，需要更多层级的路由")

	ErrPolicyNotFound        = errors.New("找不到权限信息")
	ErrNoApiPolicy           = errors.New("接口权限不足")
	ErrNoCustomerInfo        = errors.New("无法获取用户信息")
)

func init() {
	restful.RegisterErrorFromCodeAndError(20211, ErrNotActive)
	restful.RegisterErrorFromCodeAndError(20212, ErrNotActiveInvalidParam)
	restful.RegisterErrorFromCodeAndError(20213, ErrRegistFailed)

	restful.RegisterErrorFromCodeAndError(20221, ErrPolicyNotFound)
	restful.RegisterErrorFromCodeAndError(20222, ErrNoApiPolicy)
	restful.RegisterErrorFromCodeAndError(20223, ErrNoCustomerInfo)
}
