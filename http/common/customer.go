package common

import (
	"github.com/gin-gonic/gin"
)

const (
	TOKEN_CUSTOMER_INFO = "TOKEN_CUSTOMER_INFO" // 用户信息(claims.CustomerInfo)
)

// 用户信息，可以继承并放置一些业务字段
type CustomerInfo struct {
	Uid int `json:"uid,omitempty"` // 通用用户唯一标记，尽量不动这个，否则，其他的代码可能需要更改
}

// 从会话获取用户信息
func GetCustomerInfo(c *gin.Context) CustomerInfo {
	v, _ := c.Get(TOKEN_CUSTOMER_INFO)
	return v.(CustomerInfo)
}
