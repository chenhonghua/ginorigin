## jwt模块

### 功能描述

jwt-token认证功能模块。根据需要，可使用本地化(localcache)或中心化(redis)的内存存储进行token缓存，默认是本地化的。

### 文件描述

-   jwt.go
    -   主配置入口
-   token.go
    -   token对象的一些解析方法
-   handler.go
    -   接口拦截
-   cache.go
    -   缓存
-   claims.go
    -   业务内容，如过期时间、用户信息
    -   可在此处进行业务代码编写，给结构体`CustomerInfo`添加业务属性，目前，此结构体仅保存`uid`字段，作为用户唯一标记。

