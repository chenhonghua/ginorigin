package jwt

import (
	"errors"

	"gitee.com/chenhonghua/ginorigin/http/restful"
)

var (
	ErrTokenExpired     = errors.New("token is expired")
	ErrTokenNotValidYet = errors.New("token not active yet")
	ErrTokenMalformed   = errors.New("that's not even a token")
	ErrTokenInvalid     = errors.New("couldn't handle this token")
	ErrTokenNotFound    = errors.New("token cache not found")
	ErrTokenGenFailed   = errors.New("token generate failed")
	ErrTokenSignFailed  = errors.New("token sign failed")

	ErrJwtNotActive = errors.New("未启用jwt，请查看配置是否正确")
	ErrNotLogin     = errors.New("未登录或非法访问")
)

func init() {
	restful.RegisterErrorFromCodeAndError(20111, ErrJwtNotActive)
	restful.RegisterErrorFromCodeAndError(20112, ErrNotLogin)

	restful.RegisterErrorFromCodeAndError(20121, ErrTokenExpired)
	restful.RegisterErrorFromCodeAndError(20122, ErrTokenNotValidYet)
	restful.RegisterErrorFromCodeAndError(20123, ErrTokenMalformed)
	restful.RegisterErrorFromCodeAndError(20124, ErrTokenInvalid)
	restful.RegisterErrorFromCodeAndError(20125, ErrTokenNotFound)
	restful.RegisterErrorFromCodeAndError(20126, ErrTokenGenFailed)
	restful.RegisterErrorFromCodeAndError(20127, ErrTokenSignFailed)
}
