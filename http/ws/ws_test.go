package ws

import (
	"encoding/json"
	"testing"

	"gitee.com/chenhonghua/ginorigin/log"
	"github.com/gin-gonic/gin"
)

var connmanager_cid_uid map[string]string = map[string]string{}
var connmanager_uid_cid map[string]string = map[string]string{}
var connmanager_channelid_uid map[string][]string = map[string][]string{}

func TestWS(t *testing.T) {
	go manager.work()
	r := gin.Default()
	r.GET("sendMsg/:uid", GinWebsocketHandler(CustomBusyRecieveFunc1, singletonHandler))    // 长链维持
	r.GET("im/:uid/:channelid", GinWebsocketHandler(CustomBusyRecieveFunc2, imroomHandler)) // 聊天室
	r.Run(":8080")
}

var singletonHandler ShakeSuccessFunc = func(s string, c *gin.Context) {
	// uid:=jwt.GetCustomerInfo(c).Uid
	// uid := strings.Split(c.GetHeader("x-token"), ".")[1]
	log.Debugf("connmanager_cid_uid1 = %#v\n", connmanager_cid_uid)
	uid, _ := c.Params.Get("uid") // 测试类没有登录模块，使用query参数代替用户登录标记
	// log.Printf("gincontext参数：%#v\n", c)
	log.Debugf("uid = %s\n", uid)
	connmanager_cid_uid[s] = uid
	connmanager_uid_cid[uid] = s
	log.Debugf("connmanager_cid_uid2 = %#v\n", connmanager_cid_uid)
}
var imroomHandler ShakeSuccessFunc = func(s string, c *gin.Context) {
	uid, _ := c.Params.Get("uid")
	channelid, _ := c.Params.Get("channelid")
	log.Debugf("uid = %s\n", uid)
	connmanager_cid_uid[s] = uid
	connmanager_uid_cid[uid] = s
	connmanager_channelid_uid[channelid] = append(connmanager_channelid_uid[channelid], uid)
	log.Debugf("connmanager_cid_uid = %#v\n", connmanager_cid_uid)
	log.Debugf("connmanager_uid_cid = %#v\n", connmanager_uid_cid)
	log.Debugf("connmanager_channelid_uid = %#v\n", connmanager_channelid_uid)
}

// 自产自销
var CustomBusyRecieveFunc1 RecieveMessageFunc = func(msg Message) Message {
	uid := connmanager_cid_uid[msg.WsId]
	log.Debugf("1---接收到推送的消息，生产者:%s，生产者UID:%s,内容:%#v\n", msg.Sender, uid, msg)
	m := Message{
		Sender:     msg.Sender,
		Recipients: []string{msg.Sender},
		Content:    []byte("1---这是业务处理成功的消息！！！" + string(msg.Content)),
	}
	return m
}

type immsg struct {
	Channelid string `json:"channelid"`
	Content   string `json:"content"`
}

// 简易聊天室
var CustomBusyRecieveFunc2 RecieveMessageFunc = func(msg Message) Message {
	immsg := immsg{}
	json.Unmarshal(msg.Content, &immsg)
	memberids := connmanager_channelid_uid[immsg.Channelid]
	connids := make([]string, len(memberids))
	for i, mid := range memberids {
		connids[i] = connmanager_uid_cid[mid]
	}
	log.Debugf("2---接收到推送的消息, connmanager_channelid_uid = %#v, connmanager_uid_cid = %#v\n", connmanager_channelid_uid, connmanager_uid_cid)
	log.Debugf("2---接收到推送的消息, memberids = %#v, connids = %#v\n", memberids, connids)
	log.Debugf("2---接收到推送的消息，生产者:%s，收件人：%v,内容:%s\n", msg.Sender, connids, string(msg.Content))
	m := Message{
		Sender:     msg.Sender,
		Recipients: connids,
		Content:    []byte(immsg.Content),
	}
	return m
}
