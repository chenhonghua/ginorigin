package ws

import ()

var hasLoad bool

type WSConfig struct {
	Enable bool `mapstructure:"enable" json:"enable" yaml:"enable"` // 是否开启
}

func (c WSConfig) Load() {
	if hasLoad {
		return
	} else if !c.Enable {
		return
	}
	go manager.work()
	hasLoad = true
}
