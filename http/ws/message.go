package ws

type Message struct {
	WsId       string   `json:"-"`                    // 会话id
	Sender     string   `json:"sender,omitempty"`     // 发件人id -> wsid
	Recipients []string `json:"recipients,omitempty"` // 收件人id -> wsid
	Content    []byte   `json:"content,omitempty"`    // 内容
}
