package restful

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func Success(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{"code": 0})
}

func Data(ctx *gin.Context, obj interface{}) {
	ctx.JSON(http.StatusOK, gin.H{"code": 0, "data": obj})
}
