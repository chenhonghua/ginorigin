package kubernetes

import (
	"gitee.com/chenhonghua/ginorigin/log"
	k "k8s.io/client-go/kubernetes"

	"k8s.io/client-go/tools/clientcmd"
)

var (
	kubeConfig KubeConfig
	clientset  *k.Clientset
)

type KubeConfig struct {
	MasterUrl      string `yaml:"master_url" mapstructure:"master_url"`             // k8s的apiserver服务地址，格式"https://host:port"，默认情况下，apiserver的协议是https，端口是6443
	KubeConfigPath string `yaml:"kube_config_path" mapstructure:"kube_config_path"` // k8s令牌文件路径
}

func (c KubeConfig) Load() {
	if clientset != nil {
		return
	}
	kubeConfig = c
	log.Debugf("加载kubernetes配置:masterUrl=%s,kubeconfigPath=%s", c.MasterUrl, c.KubeConfigPath)
	config, err := clientcmd.BuildConfigFromFlags(c.MasterUrl, c.KubeConfigPath)
	if err != nil {
		log.Fatal(err)
	}
	clientset, err = k.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
}
