package kubernetes

import (
	"fmt"
	"log"
	"strconv"
	"testing"
	"time"

	v1 "k8s.io/api/core/v1"
)

var testConfig KubeConfig = KubeConfig{MasterUrl: "https://192.168.119.5:6443", KubeConfigPath: "/home/chenhonghua/.kube/config"}

// 测试查询全部pod
func TestGetAllPod(t *testing.T) {
	testConfig.Load()
	log.Println("=================================开始获取全部pod=================================")
	podlist, err := PodWrap.List("default")
	if err != nil {
		log.Printf("error getpod: %#v\n", err)
		return
	}
	for _, p := range podlist.Items {
		log.Printf("pod: name=%s,uid=%s\n", p.Name, p.UID)
	}
}

// 测试查询pod
func TestGetPod(t *testing.T) {
	testConfig.Load()
	log.Println("=================================开始获取pod=================================")
	pod, err := PodWrap.Get("default", "testapi")
	if err != nil {
		log.Printf("error getpod: %#v\n", err)
		return
	}
	log.Printf("pod: name=%s,uid=%s\n", pod.Name, pod.UID)
}

// 测试删除pod
func TestDelPod(t *testing.T) {
	testConfig.Load()
	log.Println("=================================开始删除pod=================================")
	namespace := "default"
	podname := "testapi"
	pod, err := PodWrap.Get(namespace, podname)
	if err != nil {
		log.Panicf("error getpod : %#v\n", err)
	}
	log.Printf("确认pod存在:%#v\n", pod)
	err = PodWrap.Delete(pod.Namespace, pod.Name)
	if err != nil {
		log.Panicf("error delpod : %#v", err)
	}
	log.Printf("删除pod成功\n")
}

// 测试创建pod
func TestCreatePod(t *testing.T) {
	testConfig.Load()
	log.Println("=================================开始创建pod=================================")
	namespace := "default"
	podname := "testapi" + time.Now().Local().Format("20060102150405")
	pod, err := NewPodAssemble(namespace, podname, "nginx").Create()
	if err != nil {
		log.Panicf("error getpod : %#v\n", err)
	}
	log.Printf("确认pod存在:name=%s,uid=%s\n", pod.Name, pod.UID)
}

// 测试创建pod，带环境变量
func TestCreatePodWithEnv(t *testing.T) {
	testConfig.Load()
	log.Println("=================================开始创建pod=================================")
	namespace := "default"
	podname := "testmounthost" + time.Now().Local().Format("20060102150405")
	pa := NewPodAssemble(namespace, podname, "nginx")
	pa = pa.EnvVar("test_env_11", "sdfsdfsdfsdfsdfdsf")
	pod, err := pa.Create()
	if err != nil {
		log.Panicf("error getpod : %#v\n", err)
	}
	log.Printf("确认pod存在:name=%s,uid=%s\n", pod.Name, pod.UID)
}

// 测试创建pod，带本地挂载
func TestCreatePodWithMountHost(t *testing.T) {
	testConfig.Load()
	log.Println("=================================开始创建pod=================================")
	namespace := "default"
	podname := "testmounthost" + time.Now().Local().Format("20060102150405")
	pa := NewPodAssemble(namespace, podname, "nginx")
	pa = pa.Volume(v1.VolumeMount{Name: "testmount1", MountPath: "/testmount1"}, v1.Volume{
		Name: "testmount1",
		VolumeSource: v1.VolumeSource{
			HostPath: &v1.HostPathVolumeSource{Path: "/nfsdata"},
		},
	})
	pod, err := pa.Create()
	if err != nil {
		log.Panicf("error getpod : %#v\n", err)
	}
	log.Printf("确认pod存在:name=%s,uid=%s\n", pod.Name, pod.UID)
}

// 测试创建pod，带nfs挂载
func TestCreatePodWithMountNfs(t *testing.T) {
	testConfig.Load()
	log.Println("=================================开始创建pod=================================")
	namespace := "default"
	podname := "testmountnfs" + time.Now().Local().Format("20060102150405")
	pa := NewPodAssemble(namespace, podname, "nginx")
	pa = pa.Volume(v1.VolumeMount{Name: "testmount1", MountPath: "/testmount1"}, v1.Volume{
		Name: "testmount1",
		VolumeSource: v1.VolumeSource{
			NFS: &v1.NFSVolumeSource{
				Server: "nfsserver",
				Path:   "/nfsdata",
			},
		},
	})
	pod, err := pa.Create()
	if err != nil {
		log.Panicf("error getpod : %#v\n", err)
	}
	log.Printf("确认pod存在:name=%s,uid=%s\n", pod.Name, pod.UID)
}

// 测试创建pod，带nfs挂载
func TestCreatePodWithMountHostNfs(t *testing.T) {
	testConfig.Load()
	log.Println("=================================开始创建pod=================================")
	namespace := "default"
	podname := "testmountnfs" + time.Now().Local().Format("20060102150405")
	pa := NewPodAssemble(namespace, podname, "nginx")
	pa = pa.Volume(v1.VolumeMount{Name: "testmount1", MountPath: "/usr/share/nginx/html"}, v1.Volume{
		Name: "testmount1",
		VolumeSource: v1.VolumeSource{
			NFS: &v1.NFSVolumeSource{
				Server: "nfsserver",
				Path:   "/nfsdata/dir222",
			},
		},
	})
	pa = pa.Port("exp1", 80)
	pod, err := pa.Create()
	if err != nil {
		log.Panicf("error getpod : %#v\n", err)
	}
	log.Printf("确认pod存在:pod.name=%s,pod.uid=%s\n", pod.Name, pod.UID)
}

func TestCreatePodWithIngress(t *testing.T) {
	testConfig.Load()
	log.Println("=================================开始创建pod=================================")
	index := 1
	times := 3
	for index < times {
		namespace := "default"
		name := "testingress-" + strconv.Itoa(index) + "-" + time.Now().Local().Format("20060102150405")
		portname := "exp1"
		port := int32(80)
		pod, err := NewPodAssemble(namespace, name, "nginx").Port(portname, port).Create()
		ingressRulePath := fmt.Sprintf("/%s%d", portname, port)
		if err != nil {
			log.Panicf("error create pod : %#v\n", err)
			return
		}
		log.Printf("确认pod存在:pod.name=%s,pod.uid=%s\n", pod.Name, pod.UID)
		svc, err := NewSvcAssemble(namespace, name, v1.ServiceTypeClusterIP).Port(portname, port).Create()
		if err != nil {
			PodWrap.Delete(namespace, name)
			log.Panicf("error create service : %#v\n", err)
			return
		}
		log.Printf("确认svc存在:svc.name=%s,svc.uid=%s\n", svc.Name, svc.UID)
		ingress, err := NewIngressAssemble(namespace, name).Rule(ingressRulePath, svc.Name, port).Create()
		if err != nil {
			PodWrap.Delete(namespace, name)
			SvcWrap.Delete(namespace, name)
			log.Panicf("error create ingress : %#v\n", err)
			return
		}
		log.Printf("确认ingress存在:ingress.name=%s,ingress.uid=%s\n", ingress.Name, ingress.UID)
		index++
	}
}
