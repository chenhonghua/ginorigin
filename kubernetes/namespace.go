package kubernetes

import (
	"golang.org/x/net/context"
	v1 "k8s.io/api/core/v1"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var NamespaceWrap namespacewrap = namespacewrap{}

type namespacewrap struct{}

func (namespacewrap) List() (*v1.NamespaceList, error) {
	return clientset.CoreV1().Namespaces().List(context.Background(), metav1.ListOptions{})
}

func (namespacewrap) Get(name string) (*v1.Namespace, error) {
	return clientset.CoreV1().Namespaces().Get(context.Background(), name, metav1.GetOptions{})
}

func (namespacewrap) Delete(name string) error {
	return clientset.CoreV1().Namespaces().Delete(context.Background(), name, metav1.DeleteOptions{})
}

func (namespacewrap) Create(name string) (*v1.Namespace, error) {
	return clientset.CoreV1().Namespaces().Create(context.Background(), &v1.Namespace{
		TypeMeta:   metav1.TypeMeta{Kind: "Namespace", APIVersion: "v1"},
		ObjectMeta: metav1.ObjectMeta{Name: name},
	}, metav1.CreateOptions{})
}
