package scheduler

import (
	"gitee.com/chenhonghua/ginorigin/log"
	"github.com/robfig/cron/v3"
)

var (
	currentJobs map[string]Job = map[string]Job{}
)

// 每隔5秒执行一次：*/5 * * * * ?
// 每隔1分钟执行一次：0 */1 * * * ?
// 每天23点执行一次：0 0 23 * * ?
// 每天凌晨1点执行一次：0 0 1 * * ?
// 每月1号凌晨1点执行一次：0 0 1 1 * ?
// 在26分、29分、33分执行一次：0 26,29,33 * * * ?
// 每天的0点、13点、18点、21点都执行一次：0 0 0,13,18,21 * * ?
type Job struct {
	Name  string
	Spec  string
	JobId int
	Func  cron.FuncJob
}

func (j Job) Start() {
	c := cron.New(cron.WithSeconds())
	cronId, err := c.AddFunc(j.Spec, j.Func)
	if err != nil {
		log.Errorf("添加定时任务时发生错误:%v\n", err)
		return
	}
	j.JobId = int(cronId)
	currentJobs[j.Name] = j
	log.Infof("添加定时任务[id=%d,name=%s]\n", j.JobId, j.Name)
	c.Start()
}
