package viper

import (
	"strings"

	"github.com/spf13/viper"
)

// 加载配置
func LoadFileToObj(path, contentType string, dest interface{}) error {
	var v *viper.Viper = viper.New()
	v.SetConfigFile(path)
	v.SetConfigType(contentType)
	err := v.ReadInConfig()
	if err != nil {
		return err
	}
	return v.Unmarshal(&dest)
}

// 加载配置
func LoadStringToObj(content,contentType string, dest interface{}) error {
	var v *viper.Viper = viper.New()
	v.SetConfigType(contentType)
	reader := strings.NewReader(content)
	err := v.ReadConfig(reader)
	if err != nil {
		return err
	}
	return v.Unmarshal(&dest)
}
