package env

import (
	"os"
	"strconv"
)

func String(envName string) (v string) {
	return os.Getenv(envName)
}
func StringVal(envName, defaultValue string) (v string) {
	v = String(envName)
	if len(v) == 0 {
		v = defaultValue
	}
	return v
}

func IntVal(envName string, defaultValue int) (v int) {
	vt := String(envName)
	if len(vt) == 0 {
		v = defaultValue
	} else {
		v, _ = strconv.Atoi(vt)
	}
	return v
}

func Int64Val(envName string, defaultValue int64) (v int64) {
	vt := String(envName)
	if len(vt) == 0 {
		v = defaultValue
	} else {
		v, _ = strconv.ParseInt(vt, 10, 64)
	}
	return v
}

func Uint64(envName string) (v uint64) {
	return Uint64Val(envName, 0)
}

func Uint64Val(envName string, defaultValue uint64) (v uint64) {
	vt := String(envName)
	if len(vt) == 0 {
		v = defaultValue
	} else {
		v, _ = strconv.ParseUint(vt, 10, 64)
	}
	return v
}

func Float64Val(envName string, defaultValue float64) (v float64) {
	vt := String(envName)
	if len(vt) == 0 {
		v = defaultValue
	} else {
		v, _ = strconv.ParseFloat(vt, 64)
	}
	return v
}

func Complex128Val(envName string, defaultValue complex128) (v complex128) {
	vt := String(envName)
	if len(vt) == 0 {
		v = defaultValue
	} else {
		v, _ = strconv.ParseComplex(vt, 64)
	}
	return v
}

func Bool(envName string) (v bool) {
	return BoolVal(envName, false)
}

func BoolVal(envName string, defaultValue bool) (v bool) {
	vt := String(envName)
	if len(vt) == 0 {
		v = defaultValue
	} else {
		v, _ = strconv.ParseBool(vt)
	}
	return v
}
