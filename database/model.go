package database

import "time"

// 自定义公共模型参数
// https://gorm.io/docs/models.html
type BaseModel struct {
	ID        uint64    `json:"id" gorm:"column:id;primarykey;"`                                                         // 开启了自增长，第一个为“1”
	CreatedAt time.Time `json:"createdAt" gorm:"column:created_at;type:TIMESTAMP;autoCreateTime;<-:create;comment:创建时间"` // 创建时会自动赋值。
	UpdatedAt time.Time `json:"updatedAt" gorm:"column:updated_at;type:TIMESTAMP;autoUpdateTime;<-;comment:更新时间"`        // 更新时会自动赋值。注：别使用UpdateColumn，否则无法触发更新时间的钩子
	DeletedAt time.Time `json:"deletedAt" gorm:"column:deleted_at;type:TIMESTAMP;comment:删除时间"`
}
