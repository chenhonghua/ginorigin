package database

import (
	"gitee.com/chenhonghua/ginorigin/log"
	"gorm.io/gorm"
)

// 获取数据库连接
func GetConnection() *gorm.DB {
	if connection == nil {
		log.Fatal("数据库连接未初始化")
	}
	return connection
}

// 事务处理，中途发生异常，进行数据回滚
func Transaction(f func(tx *gorm.DB) error) error {
	return GetConnection().Transaction(f)
}
