package database

import (
	"testing"

	connmysql "gitee.com/chenhonghua/ginorigin/database/connect/mysql"
	connsqlite "gitee.com/chenhonghua/ginorigin/database/connect/sqlite"
	"gitee.com/chenhonghua/ginorigin/env"
	log "gitee.com/chenhonghua/ginorigin/log"
)

func example_load_mysql() {
	logMode := env.StringVal("SQL_LOG_LEVEL", "debug")
	if len(logMode) == 0 {
		logMode = "error"
	}
	conn := connmysql.Mysql{
		Path:         env.StringVal("MYSQL_HOST", "localhost"),
		Port:         env.StringVal("MYSQL_PORT", "3306"),
		Dbname:       env.StringVal("MYSQL_DB", ""),
		Username:     env.StringVal("MYSQL_USER", "root"),
		Password:     env.StringVal("MYSQL_PWD", ""),
		MaxIdleConns: 1,
		MaxOpenConns: 10,
	}
	DatabaseConfig{
		Enable:   true,
		LogMode:  logMode,
		DBConfig: &conn,
	}.Load()
	log.Info("mysql database init success")
}

func TestMysql(_ *testing.T) {
	example_load_mysql()
	if tables, e := GetConnection().Migrator().GetTables(); e != nil {
		log.Fatal(e)
	} else {
		for _, t := range tables {
			log.Info(t)
		}
	}
}

func example_load_sqlite() {
	logMode := env.StringVal("SQL_LOG_LEVEL", "debug")
	if len(logMode) == 0 {
		logMode = "error"
	}
	conn := connsqlite.Sqlite{
		Path:         env.StringVal("SQLITE_HOST", "./test.db"),
		MaxIdleConns: 1,
		MaxOpenConns: 10,
	}
	DatabaseConfig{
		Enable:   true,
		LogMode:  logMode,
		DBConfig: &conn,
	}.Load()
	log.Info("mysql database init success")
}

func TestSqlite(_ *testing.T) {
	example_load_sqlite()
	type UserABC struct {
		BaseModel
		Name string
	}
	if e := GetConnection().Migrator().AutoMigrate(&UserABC{}); e != nil {
		log.Fatal(e)
	} else if tables, e := GetConnection().Migrator().GetTables(); e != nil {
		log.Fatal(e)
	} else {
		for _, t := range tables {
			log.Info(t)
		}
	}
}
