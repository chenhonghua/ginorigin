package sqlite

import (
	"database/sql"

	"gorm.io/gorm"

	sqlitedriver "gorm.io/driver/sqlite"

	_ "github.com/mattn/go-sqlite3"
)

type Sqlite struct {
	Path         string `mapstructure:"path" json:"path" yaml:"path"`                             // 服务器地址:端口
	MaxIdleConns int    `mapstructure:"max-idle-conns" json:"maxIdleConns" yaml:"max-idle-conns"` // 空闲中的最大连接数
	MaxOpenConns int    `mapstructure:"max-open-conns" json:"maxOpenConns" yaml:"max-open-conns"` // 打开到数据库的最大连接数
}

// 初始化数据库连接
func (s *Sqlite) GenConnect(gormConf *gorm.Config) (*gorm.DB, *sql.DB, error) {
	db, e := gorm.Open(sqlitedriver.Open(s.Path), gormConf)
	if e != nil {
		return nil, nil, e
	}
	sqldb, e := db.DB()
	if e != nil {
		return nil, nil, e
	} else if e = sqldb.Ping(); e != nil {
		return nil, nil, e
	}
	sqldb.SetMaxIdleConns(s.MaxIdleConns)
	sqldb.SetMaxOpenConns(s.MaxOpenConns)
	return db, sqldb, nil
}
