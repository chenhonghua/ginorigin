package connect

import (
	"database/sql"
	"errors"

	mysqldriver "gorm.io/driver/mysql"
	"gorm.io/gorm"

	"gitee.com/chenhonghua/ginorigin/log"
)

type Mysql struct {
	Path         string `mapstructure:"path" json:"path" yaml:"path"`                             // 服务器地址
	Port         string `mapstructure:"port" json:"port" yaml:"port"`                             // 端口
	Config       string `mapstructure:"config" json:"config" yaml:"config"`                       // 高级配置
	Dbname       string `mapstructure:"db-name" json:"dbname" yaml:"db-name"`                     // 数据库名
	Username     string `mapstructure:"username" json:"username" yaml:"username"`                 // 数据库用户名
	Password     string `mapstructure:"password" json:"password" yaml:"password"`                 // 数据库密码
	MaxIdleConns int    `mapstructure:"max-idle-conns" json:"maxIdleConns" yaml:"max-idle-conns"` // 空闲中的最大连接数
	MaxOpenConns int    `mapstructure:"max-open-conns" json:"maxOpenConns" yaml:"max-open-conns"` // 打开到数据库的最大连接数
}

func (m *Mysql) dsn() string {
	return m.Username + ":" + m.Password + "@tcp(" + m.Path + ":" + m.Port + ")/" + m.Dbname + "?" + m.Config
}

// 初始化数据库连接
func (m *Mysql) GenConnect(gormConf *gorm.Config) (*gorm.DB, *sql.DB, error) {
	if m.Dbname == "" {
		return nil, nil, errors.New("无法创建连接")
	}
	mysqlConfig := mysqldriver.Config{
		DSN:                       m.dsn(), // DSN data source name
		DefaultStringSize:         191,     // string 类型字段的默认长度
		SkipInitializeWithVersion: false,   // 根据版本自动配置
	}
	log.Debugf("准备创建mysql连接:%v\n", mysqlConfig)
	db, e := gorm.Open(mysqldriver.New(mysqlConfig), gormConf)
	if e != nil {
		return nil, nil, e
	}
	sqldb, e := db.DB()
	if e != nil {
		return nil, nil, e
	}

	if e = sqldb.Ping(); e != nil {
		return nil, nil, e
	}
	sqldb.SetMaxIdleConns(m.MaxIdleConns)
	sqldb.SetMaxOpenConns(m.MaxOpenConns)
	return db, sqldb, nil

}
