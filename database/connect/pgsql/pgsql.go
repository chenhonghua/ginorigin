package connect

import (
	"database/sql"
	"errors"

	"gitee.com/chenhonghua/ginorigin/log"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Pgsql struct {
	Path         string `mapstructure:"path" json:"path" yaml:"path"`                             // 服务器地址:端口
	Port         string `mapstructure:"port" json:"port" yaml:"port"`                             //:端口
	Config       string `mapstructure:"config" json:"config" yaml:"config"`                       // 高级配置
	Dbname       string `mapstructure:"db-name" json:"dbname" yaml:"db-name"`                     // 数据库名
	Username     string `mapstructure:"username" json:"username" yaml:"username"`                 // 数据库用户名
	Password     string `mapstructure:"password" json:"password" yaml:"password"`                 // 数据库密码
	MaxIdleConns int    `mapstructure:"max-idle-conns" json:"maxIdleConns" yaml:"max-idle-conns"` // 空闲中的最大连接数
	MaxOpenConns int    `mapstructure:"max-open-conns" json:"maxOpenConns" yaml:"max-open-conns"` // 打开到数据库的最大连接数
}

// Dsn 基于配置文件获取 dsn
// Author [SliverHorn](https://github.com/SliverHorn)
func (p *Pgsql) dsn() string {
	return "host=" + p.Path + " user=" + p.Username + " password=" + p.Password + " dbname=" + p.Dbname + " port=" + p.Port + " " + p.Config
}

// 初始化数据库连接
func (p *Pgsql) GenConnect(gormConf *gorm.Config) (*gorm.DB, *sql.DB, error) {
	if p.Dbname == "" {
		return nil, nil, errors.New("未选择数据库")
	}
	pgsqlConfig := postgres.Config{
		DSN:                  p.dsn(), // DSN data source name
		PreferSimpleProtocol: false,
	}
	log.Debugf("准备创建postgres连接:%v\n", pgsqlConfig)
	db, e := gorm.Open(postgres.New(pgsqlConfig), gormConf)
	if e != nil {
		return nil, nil, e
	}
	sqldb, e := db.DB()
	if e != nil {
		return nil, nil, e
	}

	if e = sqldb.Ping(); e != nil {
		return nil, nil, e
	}
	sqldb.SetMaxIdleConns(p.MaxIdleConns)
	sqldb.SetMaxOpenConns(p.MaxOpenConns)
	return db, sqldb, nil

}
