[TOC]

## GINORIGIN

### 项目描述

此项目，是以微框架gin为基础，然后，结合本人的web开发经验和java开发习惯，集成了部分常用组件，形成的web微框架。

其中，各组件，除`system`类型的模块，是系统必备模块外，其他模块，多为活动开关模块，可在config.yaml文件中，配置各模块的`Enable`值，来启用或不启用相应模块。

### ginorigin源码结构

- main.go
  
  应用主入口

- config.yaml
  
  配置文件

- docs
  
  swagger代码，自动生成，不用理睬。

- config
  
  模块代码和配置
  
   - system
     
     系统级的模块
     
      - viper
        
        `viper`是一个配置解决方案，拥有丰富的特性：
        
         - 支持 JSON/TOML/YAML/HCL/envfile/Java properties 等多种格式的配置文件；
         - 可以设置监听配置文件的修改，修改时自动加载新的配置；
         - 从环境变量、命令行选项和`io.Reader`中读取配置；
         - 从远程配置系统中读取和监听修改，如 etcd/Consul；
         - 代码逻辑中显示设置键值。
     
      - zap
        
        `zap` 是 `uber` 开源的 `Go` 高性能日志库，支持不同的日志级别， 能够打印基本信息等，但不支持日志的分割，这里我们可以使用 `lumberjack` 也是 `zap` 官方推荐用于日志分割，结合这两个库我们就可以实现以下功能的日志机制：
        
         - 能够将事件记录到文件中，而不是应用程序控制台；
         - 日志切割能够根据文件大小、时间或间隔等来切割日志文件；
         - 支持不同的日志级别，例如 `DEBUG` ， `INFO` ， `WARN` ， `ERROR` 等；
         - 能够打印基本信息，如调用文件、函数名和行号，日志时间等。
     
      - signal
        
        操作系统在关闭应用时，会发送相应信号给应用，方便应用做出相应策略。
     
      - timezone
        
        没啥好解释的，本地化时间配置。
  
   - storage
     
     数据存储相关的模块
     
      - database
        
        数据库模块，这里使用的是gorm数据框架。
        
         - 当前（2022-02-21），最新版本`v1.23`存在问题，无法执行元数据导入功能，因此，最好将gorm版本固定在`v1.20`上下。
           
           ```bash
           go get gorm.io/gorm@v1.20.11
           ```
        
         - 封装了部分方法：
           
            - 获取连接
            - 事务处理
            - 模型建表
            - 元数据初始化
            - 各种增删改查的高度封装，类似`java`的`泛型dao`
        
         - 嗯，上面的`元数据`和`泛型dao`只是设想，因为golang结构体的使用特点，导致实例传递的时候类型操作及其复杂，而这些操作在gorm里面已经进行过了，重复抽象封装会大幅消耗性能，所以，只能是想想。。。
     
      - localcache
        
        本地内存型K-V存储，并支持文件持久化。
     
      - redis
        
        redis连接模块，并提供了部分（基础操作、hash）封装方法。
     
      - oos
        
        实现ing...
  
   - http
     
     网络及部分业务功能的模块
     
      - router
        
        使用微型路由框架gin。
     
      - restful
        
        根据本人开发习惯封装的restful风格响应。
     
      - swagger
        
        swagger-api模块，配置项目是否开启swagger-api查看功能。
        
         - 在项目根目录下使用下面命令自动生成应用的swagger-api，代码在`docs`目录下。
           
           注意：windows执行可能会卡死，至少我的电脑是这样，放在linux上没问题。
           
           ```bash
           go get -u github.com/swaggo/swag/cmd/swag
           swag init --parseDependency --parseInternal
           ```
     
      - jwt
        
        jwt认证模块。用于进行登录/非登录的基本拦截功能，并预留好了自定义业务的区域`claims.go`。
     
      - kafka
        
        kafka连接模块，并封装了`生产者`和`消费者`的功能。（我用的kafka版本是`3.1.0`，不过应该不影响此模块的使用）
     
      - security
        
        资源安全功能模块，期望实现的是java的`spring-security`类似的注释权限控制。

- examples
  
  演示样例
  
   - load.go
     
     演示样例加载入口。

### 项目使用

-   #### DEMO

    #### [ginorigin-examples](https://gitee.com/chenhonghua/ginorigin-examples)

-   #### 引入项目

    ```
    go get gitee.com/chenhonghua/ginorigin
    ```

-   #### main.go

    ```go
    func main() {
    	config.RunServer(load) // 传入自定义的业务，如api、service、dao等
    }
    
    func load() {
         ......
    }
    ```

### 项目打包

- #### Linux

- ```bash
  bash build.sh
  ```

- #### Window

- ```powershell
  build.cmd
  ```

### 项目运行

#### 环境准备

- `golang 1.17`

#### 依赖

```bash
export CGO_ENABLED=0
export GOARCH=amd64
export GOOS=linux
go clean
go get
go get -u github.com/swaggo/swag/cmd/swag
swag init --parseDependency --parseInternal
```

#### 源码运行

```bash
go run main.go
# 或使用"-c"指定配置文件路径
go run main.go -c ./config.yaml
```

#### 二进制包运行

- Linux
  
   - ```bash
     ./ginorigin.linux.x86_64
     # 或使用"-c"指定配置文件路径
     ./ginorigin.linux.x86_64 -c ./config.yaml
     ```

- Window
  
   - 双击此文件运行
    
   - 或使用代码运行
    
   - ```bash
     ginorigin.windows.x86_64.exe
     # 或使用"-c"指定配置文件路径
     ginorigin.windows.x86_64.exe -c ./config.yaml
     ```



